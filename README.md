# PROJET CLASSIFICATION KNN

## Contenu de Dossiers
- corpus : Ce dossier contient trois sous-dossier 
    - culture : Ce dossier contient trois articles de culture au format txt.
    - eco : Ce dossier contient trois articles d'économie au format txt.
    - culture : Ce dossier contient trois textes de technologie au format txt.
- list : Ce dossier contient une liste de stopwords français au format txt.
- textes_a_classifier : Ce dossier contient un texte à classifier par knn au format txt.


## Class TextVect
La classe TextVect fournit une API pour lire et représenter un texte sous forme de vecteur de fréquence. Elle permet également de filtrer les stopwords et d'éliminer les mots apparaissant une seule fois (les hapax). La classe implémente également le calcul de la similarité cosinus entre deux vecteurs, ainsi que le calcul du vecteur tf_idf.

### API de la class TextVect :

Méthodes :
- __init__(self): Le constructeur de la classe TextVect. Initialisation une liste vide de stopwords et une hachage de vecteurs.
- tokenize (self,corpus,tok_grm): Tokenisation d'un texte dépendant une expression régulière. 
- read_dict (self,stoplist_filename): Lecture le stoplist.
- read_texts(self,file_names,tok_grm)-> list: Lecture d'une liste de fichiers texte, la tokenisation et le renvoi à une liste de dictionnaires avec les labels de fichiers et ses vecteurs.
- vectorise (self,tokens): Récupèration d'une liste de tokens, et le renvoi à un dictionnaire contenant le vocabulaire avec les fréquences .
- filtrage(stoplist:set, corpus, non_hapax): Supprime les mots de la liste stoplist et les mots apparaissant une seule fois (les hapax).
- cosinus(vector1,vector2): Calcul du cosinus de l'angle entre deux vecteurs.
- tf_idf(corpus): Calcule le vecteur tf_idf du texte dans le corpus.


## Class KNNClasses

Cette classe fournit une API pour créer et manipuler des classes de vecteurs pour la classification par KNN. Les vecteurs sont stockés dans une liste de classes qui contient des dictionnaires. Chaque classe a un label et une liste de vecteurs.

### API de la class KNNClasses :

Méthodes :
- __init__(self, corpus, desc="") : Le constructeur de la class KNNClasses. Initialisation d'un corpus et sa description.
- add_class(self, label, vectors) : Ajout une nouvelle classe
- add_vector(self, label, vector) : Ajout un vecteur à une classe définie par un label
- del_class(self,label) : Supprime la classe correspondant à label
- save_as_json(filename) : Cette fonction est pour enregistrer les données d'une classe au format json
- load_as_json(filename) : Cette fonction est pour charger les données depuis une classe au format json
- classify(vector,k:int,sim_func:function) : qui renvoie la liste des classes candidates pour le vecteur

## Améliorations et bogues possibles :

### Pour la classe TextVect:

Un bogue possible est que la méthode cosinus peut renvoyer une division par zéro si la longueur de l'un des vecteurs est nulle. 

Une amélioration possible est d'ajouter des fonctions pour d'autres mesures de similarité, comme la distance Euclidienne.

### Pour la classe KNNClasses:

Un bogue possible est que les messages d'erreur sont absents, ce qui peut causer des bogues dans certains cas.

Une amélioration possible est de spécifier d'une distance maximale pour la recherche de knn, afin d'accélérer la classification dans les cas où il y a un grand nombre de vecteurs dans le corpus.

