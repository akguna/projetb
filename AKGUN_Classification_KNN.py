
import math
import os
import re
import json
from typing import Tuple, Callable
import copy


#PROJET KNN

"""
class TextVect

API de la class TextVect :

Méthodes d'instance :
__init__(self): Le constructeur de la classe TextVect. Initialisation une liste vide de stopwords et une hachage de vecteurs.
tokenize (self,corpus,tok_grm): Tokenisation d'un texte dépendant une expression régulière. 
read_dict (self,stoplist_filename): Lecture le stoplist.
read_texts(self,file_names,tok_grm)-> list: Lecture d'une liste de fichiers texte, la tokenisation et le renvoi à une liste de dictionnaires avec les labels de fichiers et ses vecteurs.
vectorise (self,tokens): Récupèration d'une liste de tokens, et le renvoi à un dictionnaire contenant le vocabulaire avec les fréquences .
filtrage(stoplist:set, corpus, non_hapax): Supprime les mots de la liste stoplist et les mots apparaissant une seule fois (les hapax).
cosinus(vector1,vector2): Calcul du cosinus de l'angle entre deux vecteurs.
tf_idf(corpus): Calcule le vecteur tf_idf du texte dans le corpus.

"""

class TextVect:
  _default_encoding='utf-8'
  tok_grm = re.compile(r"""
      (?:etc\.|p\.ex\.|cf\.|M\.)|
      [a-zA-Z]+(?=(?:-(?:je|tu|ils?|elles?|nous|vous|leur|lui|les?|ce|t-|même|ci|là)))|
      [\w-]+'?| # peut-être
      [^\w\s]
  """, re.X)

  
  def __init__(self):
    self.stoplist = set()
    self.vectors= {} 
      

  def tokenize(self,corpus,tok_grm):
    """
    Input : 
      arg1 : un texte à tokeniser
      arg2 : une regex pour la tokenisation
    Output : 
      valeur de retour : une liste des tokens
    """
    return tok_grm.findall(corpus)

  def read_dict(self,stoplist_filename):
    """
      Lecture à partir d'un fichier de stoplist
      Input : 
        arg1 : str - nom du fichier à lire. Un mot par ligne.
      Output :
        valeur de retour : set(str) - ensemble des stopwords
    """
    # on ouvre, lit et après ferme le fichier
    dict_file = open(stoplist_filename, "r", encoding="utf-8")
    dict_content = dict_file.read()
    dict_file.close()
    # on sépare le dict_content(string) avec la saut de ligne et renvoie une liste
    self.stoplist = set(dict_content.split("\n"))
    return self.stoplist

  def read_texts(self,file_names,tok_grm)-> list:  
    """
    Lit une liste de fichiers texte, les tokenise et renvoie une liste de dictionnaires contenant les labels de fichiers et les vecteurs associés.
    Input:
      arg1 : file_names - Une liste de chaînes de caractères représentant les noms des fichiers à lire.
      arg2 : tok_grm - Une expression régulière pour le tokenizer.
    Outpuy:
      Une liste de dictionnaires contenant les labels de fichiers et les vecteurs associés."""
    
    vectors=[]
    for file_name in file_names :
     
        #1. tokiniser le input_file en créant une liste de tokens  
        input_file=open(file_name,mode="r",encoding="utf8") #ouvrir le fichier
        tokens=[] # initialisation de la liste  
        for line in input_file: #pour chaque ligne dans le fichier d'entrée
            line=line.strip() #on supprime les retours à la ligne
            toks=self.tokenize(line,tok_grm) #on tokise les lignes
            tokens.extend(toks) #équivalent de append mais dans les listes  
        input_file.close() #on ferme le fichier  

        # lancer vectorize sur tokens et ajouter le vecteur obtenu à la liste
        vector=self.vectorise(tokens)
        vectors.append( {'label':file_name,'vect':vector} )
    # retourner la liste
    return vectors

  def filtrage(stoplist:set, texts, non_hapax):
      """
      A partir d'une liste de documents ([{'label': , 'vect':},..}])
      on élimine tous les vocables appartenant à la stoplist.
      Input :
        arg1 : set - stoplist
        arg2 : list - un doc est un dict contenant deux clés : 'label' et 'vect'
        arg3 : bool - indique si on veut éliminer les hapax (True) ou non (False)
      """
      # on crée une liste vide comme une liste de documents avec des tokens filtrés
      documents_filtre = []
      # on parcourt chaque dictionnaire dans les dictionnaires de liste et crée un dictionnaire vide en tant que dictionnaire nouveau filtré
      for document in texts:
          document_filtre = {}
          # on met la valeur de clé "label" dans dict document à celle de document_filtre et aussi la valeur de vect "un dict" au token
          document_filtre["label"] = document["label"]
          tokens = document["vect"]
          # on crée un dictionnaire nouveau token_filtre
          tokens_filtre = {}
          # on parcourt chaque token dans les clés de dict tokens
          for token in tokens.keys():
          # selon le choix de l'utilisateur, si on veut éliminer les hapax, on exécute les codes ci-desous
              if token.lower() not in stoplist and (not non_hapax or tokens[token]>1):
                tokens_filtre[token]=tokens[token]
            
          # Après avoir terminé l'ajout des tokens dans le dictionnaire des tokens filtrés, on l'initialise dans le dictionnaire document_filtre
          document_filtre["vect"]=tokens_filtre
          # Finalement, on rajoute le dictionnaire document_filtre dans la liste des ensembles de documents filtrés (la liste documents_filtre)
          documents_filtre.append(document_filtre)
      return documents_filtre

  def vectorise(self,tokens):
    """
    Cette fonction récupère une liste de tokens, et renvoie un dictionnaire
    contenant le vocabulaire avec les fréquences associées
    Input :
      arg1 : tokens - list(str)
    Output :
      valeur de retour : un dict contenant les vocables (clés) et les fréq associées (valeur)
    """
    token_freq={}  # initialisation du hachage
    for token in tokens:  # parcours des tokens
        if token not in token_freq.keys():   # si on a un token
            token_freq[token]=0 
        token_freq[token]+=1 #on associe la fréquence à la clé (token)
    return token_freq
  
  def scalaire(vector1,vector2):
        """
    Cette fonction récupère deux vecteurs sous forme de hachage 
    et renvoie leur produit scalaire
    Input :
        arg1 : vector1 - hash
        arg2 : vector2 - hash
    Output :
        valeur de retour : un produit scalaire - float
    """
        liste_scalaire=[]
        for key in vector1:
            if key in vector2:
                liste_scalaire.append(vector1[key]*vector2[key])
        produit_scalaire=sum(liste_scalaire)
        return produit_scalaire

  def norme(vector):
        """
    Cette fonction récupère un vecteur sous forme de hachage 
    et renvoie sa norme
    Input :
        arg1 : vector - hash
    Output :
        valeur de retour : une norme - float
    """
        norme_carre=0
        for key in vector:
            norme_carre+=vector[key]*vector[key]
        norme=math.sqrt(norme_carre)
        return norme

  def cosinus(vector1,vector2):
        """
    Cette fonction récupère deux vecteurs sous forme de hachage, 
    et renvoie leur cosinus
    en appelant les fonctions Scalaire et Norme
    Input :
        arg1 : vector1 - hash
        arg2 : vector2 - hash
    Output :
        valeur de retour : un cosinus - float
    """
        norme1=TextVect.norme(vector1)
        norme2=TextVect.norme(vector2)
        scal=TextVect.scalaire(vector1,vector2)
        cosinus=(scal/(norme1*norme2))
        return cosinus


  def tf_idf (documents:list)->list:
    """
      Calcul du TF-IDF pour une liste de documents
      Input : 
        arg1 : list(dict) : une liste de documents
      Output : 
        valeur de retour : une liste de documents avec une modification des fréquences
        associées à chaque mot (on divise par le log de la fréq de documents)

    """
    documents_new=copy.deepcopy(documents)

    #création d'un dict contenant tous les mots de tous les docs
    mots=set()

    # 1. on crée l'ensemble de tous les mots
    # on parcours les documents
    for doc in documents:
      #pour chaque mot du doc étant dans notre vecteur doc
      #word = notre variable qui récupère chaque mot
      for word in doc["vect"]:
        mots.add(word)

    # 2. on parcourt tous les mots pour calculer la fréquence de doc de chacun
    freq_doc={}
    for word in mots:
      # on parcourt les documents
      for doc in documents:
        if word in doc["vect"]:
          if word not in freq_doc:
              freq_doc[word]=1
          else :
              freq_doc[word]+=1
    
    # 3. on parcourt les docs mot par mot pour mettre à jour la fréquence
    for doc in documents_new:
      for word in doc["vect"]:
        doc["vect"][word]=doc["vect"][word] / math.log(1+freq_doc[word])

    return documents_new



"""
class KNNClasses

API de la class KNNClasses

Propriétés : 
description : str
data : liste de classes qui correspond à la structure JSON
(une liste qui contient des classes -> label + ensemble de vecteurs  )

Méthodes d'instance :
__init__(self, corpus, desc="") : Le constructeur de la class KNNClasses. Initialisation d'un corpus et sa description.
add_class(self, label, vectors) : Ajout une nouvelle classe
add_vector(self, label, vector) : Ajout un vecteur à une classe définie par un label
del_class(self,label) : Supprime la classe correspondant à label
save_as_json(filename) : Cette fonction est pour enregistrer les données d'une classe au format json
load_as_json(filename) : Cette fonction est pour charger les données depuis une classe au format json
classify(vector,k:int,sim_func:function) : qui renvoie la liste des classes candidates pour le vecteur

"""

class KNNClasses:
    def __init__(self, corpus, desc=""):
        """
        arg1: corpus - une liste de classes, où chaque classe est un dictionnaire contenant un label et une liste de vecteurs.
        arg2: desc - (facultatif) une chaîne de caractères décrivant le corpus.
        """
        self.description = desc
        self.corpus = corpus
    
    def add_class(self, label, vectors):
        """ 
        arg1: label - une str représentant le label de la nouvelle classe à ajouter.
        arg2: vectors - une liste de vecteurs à ajouter à la nouvelle classe.
        """
        new_class = {"label": label, "vectors": vectors}
        self.corpus.append(new_class)
    
    def add_vector(self, label, vector): 
        """
        arg1: label - une str représentant le label de la classe où le vecteur doit être ajouté.
        arg2: vector - le vecteur à ajouter à la classe.
        """
        for c in self.corpus:
            if c["label"] == label:
                c["vectors"].append(vector)
    
    def del_class(self, label):
        """
        arg: label - de la classe à supprimer.
        """
        for i in range(len(self.corpus)):
            if self.corpus[i]["label"] == label:
                del self.corpus[i]
                return
    
    def save_as_json(self, filename):
        """ 
        arg: filename - une chaîne de caractères représentant le nom du fichier dans lequel sauvegarder le corpus.
        """
        try:
          with open(filename, "w") as f:
              json.dump({"description": self.description, "corpus": self.corpus}, f, ensure_ascii=False)
        except IOError as err:
          print ("Error")
    
    def load_as_json(self, filename):
        """
        arg: filename - une chaîne de caractères représentant le nom du fichier à partir duquel charger le corpus.
        """
        try:
          with open(filename, "r") as f:
            data = json.load(f)
        except IOError as err:
          print ("Error")
        self.description = data["description"]
        self.corpus = data["corpus"]
    
    def classify(self,vector,k=int, sim_func=TextVect.cosinus):
        """
        Input:
          arg1: vector - le vecteur à classifier.
          arg2: k - un entier représentant le nombre de voisins les plus proches à considérer pour la classification.
          arg3: sim_func - une fonction de similarité à utiliser (cosinus de la class TextVectpour comparer les vecteurs).
        Output: 
          les scores de similarités entre les classes et le nouveau texte.
          une liste d'étiquettes de classe, triée par similarité avec le vecteur d'entrée.
        """

        #hachage pour stocker les similarités entre les classes et le nouveau vecteur.
        similarites={}
        for class_ in self.corpus:
            for vec in class_["vectors"]:
                #calcul de la similarité entre le nouveau vecteur et le vecteur de la classe en cours
                sim_s = sim_func(vector, vec)

                #stockage de la similarité dans le dictionnaire similarites
                if class_["label"] in similarites:
                    similarites[class_["label"]].append(sim_s)
                else:
                    similarites[class_["label"]] = [sim_s]

        #initialisation d'une liste pour stocker les knns
        knn = []
        for label in similarites:
            #les similarités en ordre décroissant
            sims = similarites[label]
            sims.sort(reverse=True)
            #garder uniquement les knn
            if len(sims) > k:
                sims = sims[:k]
            #on ajout des knn et les scores de similarité à la liste
            knn.append({"label": label, "similarité": sum(sims)})

        #triage de la liste par similarité décroissante
        knn.sort(key=lambda x: x["similarité"], reverse=True)

        #affichage des scores de similarités pour chaque classe
        for class_ in knn:
            print(class_["label"], " - :", class_["similarité"])

        #rentrer aux labels des classes, par la similarité avec le vecteur d'entrée.
        return [class_["label"] for class_ in knn]




#EXECUTION de la class TextVect:

my_dir="/content/list"
# importation stoplist
ma_class=TextVect() 
stoplist=ma_class.read_dict(my_dir+"/stoplist.txt")
print(stoplist)

# lecture des fichiers
culture="/content/corpus/culture"
eco="/content/corpus/eco"
tech="/content/corpus/tech"
texts_culture=[culture+"/"+f for f in os.listdir(culture) if f[0]!="." and not os.path.isdir(culture+"/"+f)]
texts_eco=[eco+"/"+f for f in os.listdir(eco) if f[0]!="." and not os.path.isdir(eco+"/"+f)]
texts_tech=[tech+"/"+f for f in os.listdir(tech) if f[0]!="." and not os.path.isdir(tech+"/"+f)]
print(texts_culture)
print(texts_eco)
print(texts_tech)

# lecture des textes et vectorisation
ma_class=TextVect()
dict_culture=ma_class.read_texts(texts_culture,ma_class.tok_grm)
print(dict_culture)
dict_eco=ma_class.read_texts(texts_eco,ma_class.tok_grm)
print(dict_eco)
dict_tech=ma_class.read_texts(texts_tech,ma_class.tok_grm)
print(dict_tech)

# filtrage des vecteurs
c_vec_filtre=TextVect.filtrage(stoplist, dict_culture, False)
e_vec_filtre=TextVect.filtrage(stoplist, dict_eco, False)
t_vec_filtre=TextVect.filtrage(stoplist, dict_tech, False)
print(c_vec_filtre)
print(e_vec_filtre)
print(t_vec_filtre)

# calcul du tf.idf
tfidf_culture=TextVect.tf_idf(c_vec_filtre)
tfidf_eco=TextVect.tf_idf(e_vec_filtre)
tfidf_tech=TextVect.tf_idf(t_vec_filtre)
print(tfidf_culture)
print(tfidf_eco)
print(tfidf_tech)

#elimination des labels 
vectors_c=[v["vect"] for v in tfidf_culture]
vectors_e=[v["vect"] for v in tfidf_eco]
vectors_t=[v["vect"] for v in tfidf_tech]
print(vectors_c)
print(vectors_e)
print(vectors_t)

#EXECUTION de la classe KNNClasses:
#Classification KNN:

if __name__ == "__main__":
    
    # initialisation de classe KNNClasses
    knn = KNNClasses(corpus=[], desc="Classes Ajoutées")

    # ajout des classes en attribuant une label aux vecteurs
    knn.add_class("class_culture", vectors_c)
    knn.add_class("class_eco", vectors_e)
    knn.add_class("class_tech", vectors_t)

    # affichage de corpus actualisé
    print(knn.corpus)

    #test pour del_class
    knn.del_class("class_culture")
    # affichage de corpus actualisé et rajout de cette classe pour la suite
    print(knn.corpus)
    knn.add_class("class_culture", vectors_c)
    print(knn.corpus)

    # enregistrer le corpus en JSON
    knn.save_as_json("dict_corpus_tfidf.json")

    # charger le fichier JSON
    load_test_knn= KNNClasses([], desc="test load")
    load_test_knn.load_as_json("dict_corpus_tfidf.json")
    print(load_test_knn.corpus)


    # Analyse du nouveau texte
    # lecture du nouveau fichier
    n = "/content/textes_a_classifier"
    n_text = [n+"/"+f for f in os.listdir(n) if f[0]!="." and not os.path.isdir(n+"/"+f)]
    print(n_text)

    # lecture de texte et vectorisation
    ma_class = TextVect()
    dict_n = ma_class.read_texts(n_text, ma_class.tok_grm)
    print(dict_n)

    # filtrage de vecteur
    n_vec_filtre = TextVect.filtrage(stoplist, dict_n, False)
    print(n_vec_filtre)

    # calcul du tf.idf
    tfidf_n = TextVect.tf_idf(n_vec_filtre)
    print(tfidf_n)
    #on supprime le label
    vectors_n = [v["vect"] for v in tfidf_n]

    # CLASSIFICATION PAR KNN
    classes_prevus = knn.classify(vectors_n[0], 3, sim_func=TextVect.cosinus)
    print(classes_prevus)

